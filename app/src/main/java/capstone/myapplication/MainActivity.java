package capstone.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void ScanIDCard(View view) {//sets the button to start a new activity on click.
        Intent intent = new Intent(this, ScanIDActivity.class);
        startActivity(intent);
    }
}
