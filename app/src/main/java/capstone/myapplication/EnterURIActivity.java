package capstone.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EnterURIActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "capstone.myapplication.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_uri);
    }

//    @Override
//    public void onPause(){
//        System.exit(0);
//        super.onPause();
//    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, DrinksActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
