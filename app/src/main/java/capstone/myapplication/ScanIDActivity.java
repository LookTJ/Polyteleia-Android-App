package capstone.myapplication;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.text.ParseException;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.IOException;

public class ScanIDActivity extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 2;
    Activity thisActivity = this;
    SurfaceView cameraPreview;
    TextView barcodeResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_id);

        cameraPreview = (SurfaceView) findViewById(R.id.camera_view);
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if(rc == PackageManager.PERMISSION_GRANTED) {
            CreateCameraSource();
        } else {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                showMessageOKCancel("You need to allow access to Camera",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ScanIDActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                            }
                        });
                return;
            }
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ScanIDActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private void CreateCameraSource() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.PDF417).build();
        final CameraSource cameraSource = new CameraSource.Builder(this, barcodeDetector)
                .setAutoFocusEnabled(true)
                .setRequestedPreviewSize(1600, 1024)
                .build();

        cameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                try {
                    if (ActivityCompat.checkSelfPermission(ScanIDActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    cameraSource.start(cameraPreview.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                for (int i=0; i<barcodes.size();i++) {
                    int key = barcodes.keyAt(i);
                    final Barcode barcode = barcodes.get(key);
//                    barcodeResult.setText(barcode.format);
//                    invalidate();
                    Log.d("barcode_result", "Barcode Format: " + barcode.format);
                    Log.d("barcode_result", "Barcode RAW: " + barcode.driverLicense.birthDate);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    Date birthdate_check = null;
                    try {
                        birthdate_check = sdf.parse(barcode.driverLicense.birthDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.YEAR, -21);
                    Date age_check = cal.getTime();
                    sdf.format(age_check);
                    Log.d("barcode_result", "confirm age: " + birthdate_check);
                    if(!birthdate_check.after(age_check)) {//confirms >=21 and starts EnterURIActivity class.
                        Intent intent = new Intent(ScanIDActivity.this, EnterURIActivity.class);
                        startActivity(intent);
                    }
                    else
//                        finish();
                    {//presents message if age is <21
                        ScanIDActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),"You are not 21+",Toast.LENGTH_LONG).show();
                            }
                        });
                    }


                    break;
//                    Intent intent = new Intent(ScanIDActivity.this, ScanIDActivity.class);
//                    intent.putExtra("barcode", barcodes.valueAt(0));
//                    setResult(CommonStatusCodes.SUCCESS,intent);
//
//                    Barcode barcode = intent.getParcelableExtra("barcode");
//                    barcodeResult.setText("Barcode info :" + barcode.displayValue);
//                    finish();
                    //startActivity(intent);
                }

            }


        });
    }
}
