package capstone.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DrinksActivity extends AppCompatActivity {
    Spinner spinner;
//    Intent intent = getIntent();
//    String message = intent.getStringExtra(EnterURIActivity.EXTRA_MESSAGE);
    String URL_test;
    ArrayList<String> DrinkName;
    ArrayList<String> DrinkID;
    private Button viewBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinks);
        final Button viewBtn = (Button) findViewById(R.id.button3);

        Intent intent = getIntent();
        String message = intent.getStringExtra(EnterURIActivity.EXTRA_MESSAGE); //gets user's input from EnterURIActivity
        final String URL="http://" + message + "/drinks"; //combines to complete the API's URI
        URL_test = URL;

        DrinkName=new ArrayList<>();
        DrinkID=new ArrayList<>();
        spinner=(Spinner)findViewById(R.id.spinner);
        loadSpinnerData(URL);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String drinkname=   spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
                Toast.makeText(getApplicationContext(),drinkname,Toast.LENGTH_LONG).show();
                int id_num = spinner.getSelectedItemPosition() + 1;
                final String p_url = URL + "/" + id_num; //makes a post url based on spinner's selection
                viewBtn.setOnClickListener(new OnItemClickListener(p_url)); //sets the button
                Log.d("json_result", "confirm json: " + p_url); //debug msg
        }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
            }
        });


    }

    @Override
    public void onPause(){
        System.exit(0);
        super.onPause();
    }

    private class OnItemClickListener implements View.OnClickListener {//sets up button's click to send POST request based on arg.
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        private String post_url;

        OnItemClickListener (String position) {
            post_url = position;
        }

        @Override
        public void onClick(View arg0) {

            final String post_url_real = "http://" + URL_test + "/drinks/" + post_url;
            Log.d("Title at row" + post_url, "id");

            Map<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("param1", "1");
            JsonObjectRequest postRequest = new JsonObjectRequest( Request.Method.POST, post_url,

                    new JSONObject(),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Toast.makeText(getApplicationContext(),"Making drink",Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //   Handle Error
                            Log.e("LOG", error.toString());
                            Toast.makeText(getApplicationContext(),"Error!",Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("User-agent", System.getProperty("http.agent"));
                    return headers;
                }
            };
            requestQueue.add(postRequest);
        }}

    private void loadSpinnerData(String url) {
        RequestQueue requestQueue=Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject=new JSONObject(response);
                    if(jsonObject.getInt("success")==1){
                        JSONArray jsonArray=jsonObject.getJSONArray("drinks");
                        for(int i=0;i<jsonArray.length();i++){
                            Log.d("json_result", "confirm json: " + jsonArray.length());
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            Log.d("json_result", "confirm json: " + jsonObject1.getString("drink_name"));
                            String drinkname=jsonObject1.getString("drink_name");
                            String drinkid=jsonObject1.getString("drink_id");
                            DrinkName.add(drinkname);
                            DrinkID.add(drinkid);
                        }
                    }
                    spinner.setAdapter(new ArrayAdapter<String>(DrinksActivity.this, android.R.layout.simple_spinner_dropdown_item, DrinkName));
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }


}
